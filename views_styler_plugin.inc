<?php

/**
 * @file
 * The styler plugin of views
 *
 * The default views plugin is overriden to use this class that extend default
 * class file adding some class to each row
 */

/**
 * @defgroup views_style_plugins Views' style plugins
 * @{
 * Style plugins control how a view is rendered. For example, they
 * can choose to display a collection of fields, node_view() output,
 * table output, or any kind of crazy output they want.
 *
 * Many style plugins can have an optional 'row' plugin, that displays
 * a single record. Not all style plugins can utilize this, so it is
 * up to the plugin to set this up and call through to the row plugin.
 *
 * @see hook_views_plugins
 */

// Load wrapper file
module_load_include('inc', 'views_styler', 'views_styler.wrapper');

// Load default views_plugin_style class with different namespace
$filename  = drupal_get_path('module', 'views') . '/plugins/views_plugin_style.inc';
$namespace = "Drupal\\Modules\\Contrib\\Views";
views_styler_wrapper($filename, $namespace);

/**
 * Extend default views_plugin_style to add more classes to each row
 */
class views_plugin_style extends Drupal\Modules\Contrib\Views\views_plugin_style {

  /**
   * Options definition
   */
  function option_definition() {
    $options = parent::option_definition();

    if ($this->uses_row_class()) {
      $options['row_class_extra'] = array(
        'default' => array(
          'odd'   => '',
          'even'  => '',
          'first' => '',
          'last'  => '',
          'rows'  => array()
        )
      );
    }

    return $options;
  }

  /**
   * Options form
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if ($this->uses_row_class()) {
      $form['row_class_extra'] = array(
        '#title'         => t('Extra CSS config'),
        '#description'   => t('Add class to provide a more powerfull CSS customization.'),
        '#type'          => 'fieldset',
        '#collapsible'   => TRUE,
        '#collapsed'     => FALSE,
      );

      $form['row_class_extra']['first'] = array(
        '#title'         => t('First row class'),
        '#description'   => t('The class to provide on first row.'),
        '#type'          => 'textfield',
        '#default_value' => $this->options['row_class_extra']['first'],
      );

      $form['row_class_extra']['last'] = array(
        '#title'         => t('Last row class'),
        '#description'   => t('The class to provide on last row.'),
        '#type'          => 'textfield',
        '#default_value' => $this->options['row_class_extra']['last'],
      );

      $form['row_class_extra']['odd'] = array(
        '#title'         => t('Row class odd'),
        '#description'   => t('The class to provide on each odd row.'),
        '#type'          => 'textfield',
        '#default_value' => $this->options['row_class_extra']['odd'],
      );

      $form['row_class_extra']['even'] = array(
        '#title'         => t('Row class even'),
        '#description'   => t('The class to provide on each even row.'),
        '#type'          => 'textfield',
        '#default_value' => $this->options['row_class_extra']['even'],
      );

      $form['row_class_extra']['rows'] = array(
        '#title'       => t('Extra CSS for each row'),
        '#description' => t('Add class to provide a more powerfull CSS customization on each rows.'),
        '#type'        => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed'   => TRUE,
      );

      /**
       * @TODO: find a method to set the number of rows without using a fixed value
       */
      for ($i = 0; $i < 10; $i++) {
        $form['row_class_extra']['rows'][$i] = array(
          '#title'         => t('Class for row @index', array('@index' => $i + 1)),
          '#description'   => t('The class to provide on row @index.', array('@index' => $i + 1)),
          '#type'          => 'textfield',
          '#default_value' => $this->options['row_class_extra']['rows'][$i],
        );
      }
    }
  }

  /**
   * Return TRUE if this style uses tokens.
   *
   * Used to ensure we don't fetch tokens when not needed for performance.
   */
  function uses_tokens() {
    if ($this->uses_row_class()) {
      $class  = $this->options['row_class'];
      /**
       * @TODO: create a common function to generate class string
       */
      $class .= ' ' . implode(' ', $this->options['row_class_extra']['rows']);
      $class .= ' ' . $this->options['row_class_extra']['first'];
      $class .= ' ' . $this->options['row_class_extra']['last'];
      $class .= ' ' . $this->options['row_class_extra']['odd'];
      $class .= ' ' . $this->options['row_class_extra']['even'];

      if (strpos($class, '[') !== FALSE || strpos($class, '!') !== FALSE || strpos($class, '%') !== FALSE) {
        return TRUE;
      }
    }
  }

  /**
   * Return the token replaced row class for the specified row.
   */
  function get_row_class($row_index) {
    if ($this->uses_row_class()) {
      $extra_class = $this->options['row_class_extra'];
      $max = count($this->view->result) - 1;

      // Common class
      $class = $this->options['row_class'];

      // Add specified class for first row
      if ($row_index == 0 && $extra_class['first']) {
        $class .= ' ' . $extra_class['first'];
      }

      // Add specified class for last row
      if ($max == 0 || ($row_index == $max && $extra_class['last'])) {
        $class .= ' ' . $extra_class['last'];
      }

      // Add specified class for odd/even rows
      if ($row_class = $extra_class[($row_index % 2 ? 'odd' : 'even')]) {
        $class .= ' ' . $row_class;
      }

      // Add specified class for this row.
      // This is the last class added to override previews
      if (isset($extra_class['rows'][$row_index])) {
        $class .= ' ' . $extra_class['rows'][$row_index];
      }

      // Tokenize class (if available)
      if ($this->uses_fields() && $this->view->field) {
        $class = $this->tokenize_value($class, $row_index);
      }

      // Normalize CSS class output
      if ($class) {
        $classes = explode(' ', $class);
        foreach ($classes as &$class) {
          $class = drupal_clean_css_identifier($class);
        }
        return implode(' ', $classes);
      }
    }
  }
}

/**
 * @}
 */
