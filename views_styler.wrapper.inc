<?php

/**
 * @file
 * The styler plugin of views
 *
 * The default views plugin is overriden to use this class that extend default
 * class file adding some class to each row, this file create a wrapper to load
 * the original class in another namespace.
 */

/**
 * Put a class into different namespace to be able to create a class with the
 * same name to extend orginal class.
 *
 * @param string $filename
 *   The path of the file where class is located.
 * @param string $namespace
 *   The namespace where move the loaded class
 */
function views_styler_wrapper($filename, $namespace) {
  // Open file
  $line = file_get_contents($filename);

  // Insert namespace into class
  $line = str_replace("<?php", "namespace $namespace;", $line);
  $line = str_replace("extends ", "extends \\", $line);

  // Generate class from code
  // We can also use php_eval but add php module dependence.
  eval($line);
}
